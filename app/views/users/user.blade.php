@extends('layout.main')

@section('content')

  <h2>Info About User: {{ $user->username }}</h2>

  Username: {{ $user->username }}<br>
  Email: {!! Html::mailto($user->email) !!}

@stop

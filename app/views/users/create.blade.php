@extends('layout.main')

@section('content')

  @if(Session::has('msg'))
    <div class="alert alert-info alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
      {{ Session::flash('msg') }}
    </div>
  @endif

  <div class="well">
    {!! Form::open(['class' => 'form-horizontal']) !!}
    <fieldset>
      <legend>Registration Form</legend>
      @if($errors->has('username') && !empty($errors->get('username')))
        <div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
          @if(is_array($errors->get('username')))
            @foreach($errors->get('username') as $error)
              {{ $error }}<br>
            @endforeach
          @else
            {{ $errors->first('username') }}
          @endif
        </div>
      @endif
      <div class="form-group">
        {!! Form::label('username', 'Username:', ['class' => 'col-lg-2']) !!}
        <div class="col-lg-10">
          {!! Form::text('username', Session::input('username'), ['class' => 'form-control']) !!}
        </div>
      </div>

      @if($errors->has('password') && !empty($errors->get('password')))
        <div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
          @if(is_array($errors->get('password')))
            @foreach($errors->get('password') as $error)
              {{ $error }}<br>
            @endforeach
          @else
            {{ $errors->first('password') }}
          @endif
        </div>
      @endif
      <div class="form-group">
        {!! Form::label('password', 'Password:', ['class' => 'col-lg-2']) !!}
        <div class="col-lg-10">
          {!! Form::password('password', '', ['class' => 'form-control']) !!}
        </div>
      </div>

      @if($errors->has('password_again') && !empty($errors->get('password_again')))
        <div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
          @if(is_array($errors->get('password_again')))
            @foreach($errors->get('password_again') as $error)
              {{ $error }}<br>
            @endforeach
          @else
            {{ $errors->first('password_again') }}
          @endif
        </div>
      @endif
      <div class="form-group">
        {!! Form::label('password_again', 'Password Again:', ['class' => 'col-lg-2']) !!}
        <div class="col-lg-10">
          {!! Form::password('password_again', '', ['class' => 'form-control']) !!}
        </div>
      </div>

      @if($errors->has('email') && !empty($errors->get('email')))
        <div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
          @if(is_array($errors->get('email')))
            @foreach($errors->get('email') as $error)
              {{ $error }}<br>
            @endforeach
          @else
            {{ $errors->first('email') }}
          @endif
        </div>
      @endif
      <div class="form-group">
        {!! Form::label('email', 'E-Mail Address:', ['class' => 'col-lg-2']) !!}
        <div class="col-lg-10">
          {!! Form::text('email', Session::input('email'), ['class' => 'form-control']) !!}
        </div>
      </div>

      {!! Form::submit('Create User', ['class' => 'btn btn-primary']) !!}

      {!! Form::close() !!}
    </fieldset>
  </div>

  {!! Html::link('/users/login', 'Login') !!}

@stop

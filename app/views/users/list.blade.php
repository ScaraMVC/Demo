@extends('layout.main')

@section('content')

  <h2>Pagination Test</h2>
  @if($users->count() > 0)
    @foreach($users->get() as $user)
      {!! Html::link("/users/{$user->username}", $user->username) !!}<br>
    @endforeach
    {!! $users->render('bootstrap') !!}
  @else
    There are currently no registered users!
  @endif

@stop

@extends('layout.main')

@section('content')

  @if(Session::has('msg'))
    <div class="alert alert-info alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
      {{ Session::flash('msg') }}
    </div>
  @endif

  Welcome, {{ $user->username }}
  <br><br>
  {!! Html::link('/users/logout', 'Logout') !!}

@stop

@if(Auth::check())
  <li>{!! Html::link('/users', 'Portal Home') !!}</li>
  <li>{!! Html::link('/users/edit', 'Edit Profile') !!}</li>
  <li>{!! Html::link('/users/logout', 'Logout') !!}</li>
@else
  <li>{!! Html::link('/users/login', 'Login') !!}</li>
  <li>{!! Html::link('/users/create', 'Register') !!}</li>
@endif

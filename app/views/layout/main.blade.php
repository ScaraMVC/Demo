<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title>Scara MVC</title>
    <link rel="stylesheet" href="{{ asset('css/site.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/zenburn.css') }}" type="text/css">
    <script src="{{ asset('js/global.min.js') }}" type="text/javascript"></script>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="navbar-inner">
        <div class="container">
          <a href="{{ url('/') }}" class="navbar-brand">Scara PHP MVC Framework</a>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Mobile Menu</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav navbar-right">
              <li>{!! link_to('/about', 'About') !!}</li>
              <li>{!! link_to('/docs', 'Documentation') !!}</li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User Portal <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li>{!! Html::link('/users/list', 'User List') !!}</li>
                  @include('modules.portallinks')
                </ul>
              </li>
              @if(Auth::check())
                <li>
                  <p class="navbar-text navbar-right">Signed in as {!! Html::link('/users/'.Auth::user()->username, Auth::user()->username, ['class' => 'navbar-link']) !!}</p>
                </li>
              @endif
            </ul>
          </nav>
        </div>
      </div>
    </nav>
    <div class="pageContent">
      <div class="container">
        <div class="col-lg-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Forward
            </div>
            <div class="panel-body">
              <p>Thanks for checking out Scara! Please feel free to play around with the demo site, or {!! Html::link('/about', 'check out what it\'s all about') !!}</p>
              <hr>
              <p>Check out Scara over at OpenHub:</p>
              <script type='text/javascript' src='https://www.openhub.net/p/Scara/widgets/project_factoids_stats?format=js'></script>
              <p>Use Scara? Let me know!</p>
              <script type='text/javascript' src='https://www.openhub.net/p/Scara/widgets/project_users?format=js&style=green'></script>
            </div>
          </div>
        </div>
        <div class="col-lg-8">
          @yield('content')
        </div>
      </div>
    </div>
    <div class="footer navbar-default navbar-footer">
      <div class="container">
        <div class="footer_text">
          Site &copy; <span class="date">{{ date('Y') }}</span>
          {!! Html::link('https://kalebklein.com', 'Kaleb Klein') !!}
          |
          {!! Html::link('https://validator.w3.org/check?uri=referer', 'Valid HTML5') !!}
          |
          {{ Benchmark::check('scara') }} ms
          |
          {{ Benchmark::memcheck() }} mb
        </div>
      </div>
    </div>
  </body>
</html>

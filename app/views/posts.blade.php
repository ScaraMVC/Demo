@extends('layout.main')

@section('content')

<h2>Model Relationship Test</h2>

<h3>{{ $post->title }}</h3>
<p>{{ $post->content }}</p>
<hr>
<h4>Comments</h4>

@foreach($comments as $comment)
    Poster: {{ $comment->poster }}<br>
    Comment: {{ strip_tags($comment->content) }}<br>
    Post ID: {{ $comment->blog_post_id }}<br><br>
@endforeach

@stop

@extends('layout.main')

@section('content')
  <style type="text/css">
  .codeBlock
  {
      background: #dae4ed;
      border: 1px solid #0D8EFF;
      color: #000;
      padding: 5px;
      font-family: monospace;
      overflow: auto;
      font-size: 12px;
      max-height: 300px;
  }

  .codeBlockHeader
  {
      color: green;
      margin-bottom: -15px;
      font-weight: bold;
      padding: 10px 10px 20px;
      background: url('http://cdn.kalebklein.com/kparser/img/cbh.png');
  }
  </style>
  <h2>About Scara</h2>
  <hr>
  {!! KParser::parse("[p]Scara is a simple MVC framework built in PHP. It's influences stem from the likes of Laravel, Symfony, and CakePHP. Scara even uses some of their libraries. Specifically, Laravel's Blade templating engine and Database libraries, along with Symfony's debugging and console libraries.[/p]

[p]Scara is still a very young framework, and is far from complete. Here's a list of a few things it DOES support:[/p]

[ul]
[li]Sessions (Both PHP session and file sessions utilizing JSON (Cookies are broken. And it's getting annoying. Haha))[/li]
[li]Databases (Migrations, models, etc. powered by Laravel's database framework)[/li]
[li]Pagination (The number of results to a query with page numbers)[/li]
[li]Views/Controllers (For loading pages based on URLs while loading in PHP classes and methods)[/li]
[li]Models (For getting data from the database easily. Models are table dependent. Soon, they'll have inheritance as well)[/li]
[li]Advanced routing (Handles GET/POST requests with request bags. Described next)[/li]
[li]Request handling (Routes with custom URL variables and POST request variable handling)[/li]
[li]Terminal commands (for quickly creating database migrations/controllers/clearing session/view cache)[/li]
[li]User authentication (Easy authentication built in. No need to learn user login/session management. It's already built in)[/li]
[li]Custom application/database configuration scripts that are easy to maintain[/li]
[li]HTML and Form building classes (for calling a function to generate entire HTML scripts)[/li]
[li]Form/Request validation (Validating form submissions with ability to translate into any language)[/li]
[li]\"Class Facades\" (Class aliases for quick calling of class functions in either views or controllers)[/li]
[li]String encryption using the BCrypt algorithm)[/li]
[li]Debugging (Easy to read error/exception handling pages)[/li]
[li]Custom Error Pages (If you need to set up a custom error page. Like \"page not found\" or something)[/li]
[li]As you can tell, it's quite a list, and still growing. It's still not a practical use thing yet, but it's getting there.[/li]
[/ul]

[p]Here are a couple more links on it. The API site (describing all callable methods/classes for the core framework), and Github sites for both the application code, and the core framework.[/p]

Documentation: [url=http://api.kalebklein.com/scara/docs newtab]http://api.kalebklein.com/scara/docs[/url][nl]
API Reference: [url=http://api.kalebklein.com/scara/docs/reference newtab]http://api.kalebklein.com/scara/docs/reference[/url][nl]
Application Code: [url=https://github.com/ScaraMVC/Scara newtab]https://github.com/ScaraMVC/Scara[/url][nl]
Framework Code: [url=https://github.com/ScaraMVC/Framework newtab]https://github.com/ScaraMVC/Framework[/url][nl]
[nl]

Example Route:
[code=php]\$router->post('/users/login', 'UsersController@postLogin'); // Post request to login page[/code]

Example Login:
[code=php]...
use Scara\Http\Controller; // base controller class
use Scara\Routing\Request; // For handling requests (POST in this case)
use App\Model\User; // The user model
use Validator; // Validator alias (using facade)
use Input; // Input alias (for grabbing form input)
use Hash; // Hash alias (for hashing passwords)
class UsersController extends Controller {
    public function postLogin(Request \$request)
    {
        // validation rules. Username is required, and we're checking against the db to make sure it exists
        // password is required
        \$rules = [
            'username'  => 'required|from:users:username',
            'password'  => 'required'
        ];

        \$valid = Validator::make(Input::all(), \$rules); // generate validation

        // check if validation passed
        if(\$valid->isValid())
        {
            // attempt to get the user via supplied username
            \$user = User::init()->where('username', '=', Input::get('username'))->first();

            // check if hashed password is verifiable against the new hash of the submitted password
            if(Hash::check(Input::get('password'), \$user->password))
            {
                // Attempt to authenticate the user using the built in Authentication
                \$login = Auth::attempt([
                    'username' => \$user->username,
                    'password' => \$user->password
                ]);

                // If successfull, redirect to users home page with success message. Else, there's an error
                if(\$login)
                    \$this->flash('msg', 'You have successfully logged in!')->redirect('/users');
                else
                    \$this->flash('msg', 'There was an error logging you in. Please try again later')->redirect('/users/login');
            }
            else
            {
                // User supplied incorrect login information. Redirect to login form with input and message
                \$this->flash('msg', 'Invalid username/password combination!')->withInput()->redirect('/users/login');
            }
        }
        else
        {
            // Form validation failed. Redirect to login form with input and message
            \$this->errors(\$valid)->withInput()->redirect('/users/login');
        }
    }
}
...[/code]

[p]Creating migrations and controllers is pretty easy. Let's create this controller and a database migration to go with the above code. (The controller command will generate the skeleton code for the controller above)[/p]
[terminal user=kaleb host=testsites.kalebklein.com theme=bluebird]
[command]php cli db:make create_users_table[/command]
[response]Created migration: migrations/2016_09_03_042429_create_users_table.php successfully.<br>[/response]
[command]php cli controller create UsersController[/command]
[response]Controller: UsersController created.[/response]
[/terminal]

[p]While there's quite a bit to offer already, there's still more to add/remove and still isn't ready for practical use. If you'd like to play around with the framework, by all means, go on! :bigsmile:[/p]") !!}
@stop

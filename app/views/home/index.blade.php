@extends('layout.main')

@section('content')

  <?php $display = <<<EOF
[h2]Scara MVC[/h2]
[h3]Introduction[/h3]
[p]Scara is an MVC framework built in PHP. Scara is made to be a small, easy to use framework with influences from the likes of Laravel, Symfony, and Lithium[/p]

[p]This site is demo site used to test out various features to the Scara API. Use the links at the top to play round with the demo site, or check out the official documentation.[/p]

[hr]

[h3]Resources[/h3]
[p]The various resources for Scara can be found below[/p]

[ul]
[li][url=http://api.kalebklein.com/scara/docs]Official Scara Documentation[/url] (Not Complete)[/li]
[li][url=http://api.kalebklein.com/scara/docs/reference newtab]Scara API Reference[/url][/li]
[li][url=https://github.com/ScaraMVC/Scara newtab]Scara on GitHub[/url][/li]
[li][url=https://packagist.org/packages/scaramvc/scara newtab]Scara on Packagist[/url][/li]
[/ul]
EOF;
?>

    {!! KParser::parse($display) !!}

    Scara's Current Version: {{ version() }}

@stop

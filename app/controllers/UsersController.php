<?php

namespace App\Controllers;

use App\Models\User;
use Auth;
use Hash;
use Input;
use Scara\Http\Controller;
use Scara\Http\Request;
use Validator;

class UsersController extends Controller
{
    // ======== POST REQUEST METHODS
    public function postCreateUser(Request $request)
    {
        $rules = [
            'username'          => 'required',
            'password'          => 'required|min:6|max:12',
            'password_again'    => 'required|match:password',
            'email'             => 'required|email',
        ];

        $valid = Validator::make(Input::all(), $rules);

        if ($valid->isValid()) {
            $user = User::init()->create([
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'email'    => $request->email,
            ]);

            if ($user) {
                $this->flash('msg', 'User successfully created!')->redirect('/users/login');
            } else {
                $this->flash('msg', 'Error creating user')->redirect('/users/create');
            }
        } else {
            $this->errors($valid)->withInput()->redirect('/users/create');
        }
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'username'  => 'required|from:users:username',
            'password'  => 'required',
        ];

        $valid = Validator::make(Input::all(), $rules);

        if ($valid->isValid()) {
            $user = User::init()->where('username', '=', Input::get('username'))->first();

            if (Hash::check(Input::get('password'), $user->password)) {
                $login = Auth::attempt([
                    'username' => $user->username,
                    'password' => $user->password,
                ]);

                if ($login) {
                    $this->flash('msg', 'You have successfully logged in!')->redirect('/users');
                } else {
                    $this->flash('msg', 'There was an error logging you in. Please try again later')->redirect('/users/login');
                }
            } else {
                $this->flash('msg', 'Invalid username/password combination!')->withInput()->redirect('/users/login');
            }
        } else {
            $this->errors($valid)->withInput()->redirect('/users/login');
        }
    }

    // ======== GET REQUEST METHODS
    public function getIndex()
    {
        $data = [
            'user' => Auth::user(),
        ];

        if (Auth::check()) {
            $data = [
                'user' => Auth::user(),
            ];
            $this->renderWithData('users.home', $data);
        } else {
            $this->redirect('/users/login');
        }
    }

    public function getLogin()
    {
        if (Auth::check()) {
            $this->redirect('/users');
        } else {
            $this->render('users.login');
        }
    }

    public function getLogout()
    {
        if (Auth::check()) {
            if (Auth::logout()) {
                $this->flash('msg', 'You have successfully logged out')->redirect('/users');
            } else {
                $this->flash('msg', 'Failed to logout')->redirect('/users');
            }
        } else {
            $this->redirect('/users/login');
        }
    }

    public function getCreateUser()
    {
        if (Auth::check()) {
            $this->redirect('/users');
        } else {
            $this->render('users.create');
        }
    }

    public function getUser(Request $request)
    {
        $user = User::init()->where('username', '=', $request->username);

        if ($user->count() > 0) {
            $user = $user->first();
            $this->with('user', $user)->render('users.user');
        } else {
            $this->with('msg', "The user {$request->username} could not be found!")
            ->render('users.nouser');
        }
    }

    public function getUsers()
    {
        $data = [
            'users' => User::init()->paginate(2),
        ];
        $this->renderWithData('users.list', $data);
    }
}

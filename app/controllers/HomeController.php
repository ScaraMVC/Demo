<?php

namespace App\Controllers;

use Scara\Http\Controller;
use Scara\Http\Request;
use App\Models\BlogPost;

class HomeController extends Controller
{
    /**
     * The method loaded from the default home page.
     *
     * @return void
     */
    public function index()
    {
        $this->render('home.index');
    }

    /**
     * The method loaded from the about page.
     *
     * @return void
     */
    public function about()
    {
        $this->render('home.about');
    }

    /**
     * Method for testing model relationships.
     *
     * @param \Scara\Http\Request $request
     *
     * @return void
     */
    public function getBlogPost(Request $request)
    {
        $bp = BlogPost::init();
        $post = $bp->find($request->id);

        $comments = $bp->comments($post->id);

        $this->renderWithData('posts', ['post' => $post, 'comments' => $comments]);
    }
}

<?php

namespace App\Models;

use Scara\Http\Model;

class Comment extends Model
{
    // Table name here
    protected $table = 'comments';

    // Mutable fields here
    protected $fillable = ['blog_post_id', 'poster', 'content'];
}

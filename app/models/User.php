<?php

namespace App\Models;

use Scara\Http\Model;

class User extends Model
{
    protected $fillable = ['username', 'password', 'email'];

    protected $table = 'users';
}

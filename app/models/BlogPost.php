<?php

namespace App\Models;

use Scara\Http\Model;

class BlogPost extends Model
{
    // Table name here
    protected $table = 'blog_posts';

    // Mutable fields here
    protected $fillable = ['title', 'content'];

    public function comments($id)
    {
        return $this->getChild('App\Models\Comment')->where('blog_post_id', '=', $id)->get();
    }
}

<?php

/**
 * All your routes are defined here.
 * Use the router to define your routes.
 *
 * Routes can be defines in several ways
 *
 * $router->get() defines a GET request
 * $router->post() defines a POST request
 *
 * You can define a route's controller 2 ways
 * Either use just the controller/method as the
 * action, or define the action as an array.
 *
 * The controller action is defined by Controller@method
 *
 * Each route needs a URL to load, the first parameter
 * loads the URL, or the page to load
 *
 * The second parameter is the action
 */

// This route is a GET request for the index
// It's using the array action, calling the controller
// via the 'uses' index
$router->get('/', [
    'uses' => 'HomeController@index',
]);

$router->get('/about', 'HomeController@about');

// user portal routes
$router->get('/users', 'UsersController@getIndex');
$router->get('/users/create', 'UsersController@getCreateUser');
$router->get('/users/login', 'UsersController@getLogin');
$router->get('/users/logout', 'UsersController@getLogout');
$router->get('/users/list', 'UsersController@getUsers');
$router->get('/users/edit', 'UsersController@getEdit');
$router->get('/users/{username}', 'UsersController@getUser');

$router->post('/users/create', 'UsersController@postCreateUser');
$router->post('/users/login', 'UsersController@postLogin');
$router->post('/users/edit', 'UsersController@postEdit');

$router->get('/testblog/post/{id}', 'HomeController@getBlogPost');

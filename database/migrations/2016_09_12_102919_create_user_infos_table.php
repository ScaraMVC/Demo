<?php

use Scara\Database\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * For pushing migrations up.
     *
     * @return void
     */
    public function up()
    {
        $this->create('userinfo', function ($table) {
            // Place table rows here
            $table->increments('id');
        });
    }

    /**
     * For reversing migrations.
     *
     * @return void
     */
    public function down()
    {
        // This is for removing the table
        $this->drop('userinfo');
    }
}

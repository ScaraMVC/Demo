<?php

use Scara\Database\Blueprint;
use Scara\Database\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * For pushing migrations up
     *
     * @return void
     */
    public function up()
    {
        $this->create('comments', function($table)
        {
            // Place table rows here
            $table->increments('id');
            $table->integer('blog_post_id');
            $table->string('poster');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * For reversing migrations
     *
     * @return void
     */
    public function down()
    {
        // This is for removing the table
        $this->drop('comments');
    }
}

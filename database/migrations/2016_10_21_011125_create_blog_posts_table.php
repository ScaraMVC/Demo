<?php

use Scara\Database\Blueprint;
use Scara\Database\Migration;

class CreateBlogPostsTable extends Migration
{
    /**
     * For pushing migrations up
     *
     * @return void
     */
    public function up()
    {
        $this->create('blog_posts', function($table)
        {
            // Place table rows here
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * For reversing migrations
     *
     * @return void
     */
    public function down()
    {
        // This is for removing the table
        $this->drop('blog_posts');
    }
}

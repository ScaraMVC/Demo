<?php

use App\Models\BlogPost;
use App\Models\Comment;

/**
 * BlogCommentSeeder seed test.
 */
class BlogCommentSeeder
{
    /**
     * run method is executed when running the seeder.
     *
     * @return void
     */
    public function run()
    {
        $title = 'Post title - '.time();
        BlogPost::init()->create([
            'title' => $title,
            'content' => 'Lorem ipsum Aliquip amet laborum pariatur anim sed cillum ex cupidatat exercitation aute reprehenderit sunt sunt deserunt veniam Duis nulla id tempor culpa deserunt eu esse dolore labore.',
        ]);

        $post = BlogPost::init()->where('title', '=', $title)->first();

        for($i = 0; $i < 10; $i++)
        {
            Comment::init()->create([
                'poster' => 'Test Poster - ' . ($i + 1),
                'blog_post_id' => $post->id,
                'content' => 'Lorem ipsum Officia velit est esse eu consectetur consectetur consectetur ut esse non ullamco labore enim pariatur do incididunt sunt minim dolore.',
            ]);
        }
    }
}
